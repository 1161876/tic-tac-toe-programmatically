//
//  UnbeatableAITests.swift
//  Tic_tac_toe_No_StoryboardTests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class UnbeatableAITests: XCTestCase {

	
	let gameField = GameField()
	
    override func setUp() {
		super.setUp()
		gameField.cleanGameField()
    }
	
	/* test this situation
	 - - -
	 - o -
	 - - -
	*/
	func testAIShouldPickBestSpot(){
		gameField.setPlayerSpot(index: 4, player: Player.human)
		let aIMove = UnbeatableAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 0)
	}
	
	/* test this situation
	 x o -
	 x o -
	 - - -
	*/
	func testAIShouldWin(){
		gameField.setPlayerSpot(index: 1, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		gameField.setPlayerSpot(index: 0, player: Player.aI)
		gameField.setPlayerSpot(index: 3, player: Player.aI)
		let aIMove = UnbeatableAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 6)
	}
	
	/* test this situation
	 o x -
	 x o -
	 - - -
	*/
	func testAIShouldDefend(){
		gameField.setPlayerSpot(index: 0, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		gameField.setPlayerSpot(index: 1, player: Player.aI)
		gameField.setPlayerSpot(index: 3, player: Player.aI)
		let aIMove = UnbeatableAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 8)
	}
	
}
