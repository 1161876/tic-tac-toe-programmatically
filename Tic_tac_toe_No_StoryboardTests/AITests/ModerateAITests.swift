//
//  ModerateAITests.swift
//  Tic_tac_toe_No_StoryboardTests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class ModerateAITests: XCTestCase {
	
	let gameField = GameField()
	
    override func setUp() {
		super.setUp()
		gameField.cleanGameField()
    }

	/* test this situation
	 o - -
	 - - -
	 - - -
	*/
	func testAIShouldPicRandomFreeSpot(){
		gameField.setPlayerSpot(index: 0, player: Player.human)
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertNotEqual(aIMove, 0)
	}
	
	
	/* test this situation
	 x x -
	 o o -
	 - - -
	*/
	func testAIShouldSelectWinningMoveInLine(){
		gameField.setPlayerSpot(index: 3, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		gameField.setPlayerSpot(index: 0, player: Player.aI)
		gameField.setPlayerSpot(index: 1, player: Player.aI)
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 2)
	}
	
	/* test this situation
	 x o -
	 x o -
	 - - -
	*/
	func testAIShouldSelectWinningMoveInColumn(){
		gameField.setPlayerSpot(index: 1, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		gameField.setPlayerSpot(index: 0, player: Player.aI)
		gameField.setPlayerSpot(index: 3, player: Player.aI)
		
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 6)
	}
	
	/* test this situation
	 x o -
	 o x -
	 - - -
	*/
	func testAIShouldSelectWinningMoveInLeftDiagonal(){
		gameField.setPlayerSpot(index: 1, player: Player.human)
		gameField.setPlayerSpot(index: 3, player: Player.human)
		gameField.setPlayerSpot(index: 0, player: Player.aI)
		gameField.setPlayerSpot(index: 4, player: Player.aI)
		
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 8)
	}
	
	/* test this situation
	 - o x
	 - x o
	 - - -
	*/
	func testAIShouldSelectWinningMoveInRightDiagonal(){
		gameField.setPlayerSpot(index: 1, player: Player.human)
		gameField.setPlayerSpot(index: 5, player: Player.human)
		gameField.setPlayerSpot(index: 2, player: Player.aI)
		gameField.setPlayerSpot(index: 4, player: Player.aI)
		
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 6)
	}

	/* test this situation
	 x - -
	 o o -
	 - - -
	*/
	func testAIShouldDefendInLine(){
		gameField.setPlayerSpot(index: 0, player: Player.aI)
		gameField.setPlayerSpot(index: 3, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 5)
	}
	
	/* test this situation
	 x o -
	 - o -
	 - - -
	*/
	func testAIShouldDefendInColumn(){
		gameField.setPlayerSpot(index: 0, player: Player.aI)
		gameField.setPlayerSpot(index: 1, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 7)
	}
	
	/* test this situation
	 o x -
	 - o -
	 - - -
	*/
	func testAIShouldDefendInLeftDiagonal(){
		gameField.setPlayerSpot(index: 1, player: Player.aI)
		gameField.setPlayerSpot(index: 0, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 8)
	}
	
	/* test this situation
	 - x o
	 - o -
	 - - -
	*/
	func testAIShouldSDefendInRightDiagonal(){
		gameField.setPlayerSpot(index: 1, player: Player.aI)
		gameField.setPlayerSpot(index: 2, player: Player.human)
		gameField.setPlayerSpot(index: 4, player: Player.human)
		
		let aIMove = ModerateAI().getNextMove(gameField: gameField)
		XCTAssertEqual(aIMove, 6)
	}


}
