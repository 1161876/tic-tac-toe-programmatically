//
//  GameViewControllerTests.swift
//  Tic_tac_toe_No_StoryboardTests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class GameViewControllerTests: XCTestCase {

	let gameVC = GameViewController()
	
	override func setUp() {
		super.setUp()
		//Make sure that the userDefaults value is set to false to force Light mode
		UserDefaults.standard.set(false, forKey: "isDarkMode")
		gameVC.beginAppearanceTransition(true, animated: false)
		gameVC.endAppearanceTransition()
    }
	
	//test light mode
	func testLightMode(){
		XCTAssertFalse(gameVC.isDarkMode)
	}
}
