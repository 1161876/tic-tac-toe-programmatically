//
//  GameTests.swift
//  Tic_tac_toe_No_StoryboardTests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class GameTests: XCTestCase {

	var game = Game(subscriber: MockGameSubscriber())
	
	func testGameStartsWithNineFreeSpots(){
		let gameField = game.getGameField()
		XCTAssertEqual(gameField.getFreeSpots().count, 9)
	}
	
	func testNewGameShouldCleanGameField(){
		//Select some positions
		let selectionSequence = [0,3,1,4,2]
		setPlayersSelections(playersSelectionsSequence: selectionSequence)
		//Clean the game field
		game.newGame()
		//make sure that the game field was cleaned
		let gameField = game.getGameField()
		XCTAssertEqual(gameField.getFreeSpots().count, 9)
	}

	
	/* test this situation
	 o o o
	 x x -
	 - - -
	*/
	func testIfGameDetectsPlayerWonWithLineCombination(){
		//Assert that we have no winner in the beginning
		XCTAssertNil(game.getWinner())
		
		let selectionSequence = [0,3,1,4,2]
		setPlayersSelections(playersSelectionsSequence: selectionSequence)
		
		//We expect circle to win, in this case, the AI
		XCTAssertEqual(game.getWinner(), Player.human)
	}
	
	/* test this situation
	 x o o
	 x o -
	 x - -
	*/
	func testIfGameDetectsPlayerWonWithColumnCombination(){
		//Assert that we have no winner in the beginning
		XCTAssertNil(game.getWinner())
		
		let selectionSequence = [1,0,4,3,2,6]
		setPlayersSelections(playersSelectionsSequence: selectionSequence)
		
		//We expect the cross to win, in this case, the human player
		XCTAssertEqual(game.getWinner(), Player.aI)
	}
	
	/* test this situation
	 o - -
	 x o -
	 x - o
	*/
	func testIfGameDetectsPlayerWonWithRightDiagonalCombination(){
		//Assert that we have no winner in the beginning
		XCTAssertNil(game.getWinner())
		
		let selectionSequence = [0,3,4,6,8]
		setPlayersSelections(playersSelectionsSequence: selectionSequence)
		
		//We expect circle to win, in this case, the AI
		XCTAssertEqual(game.getWinner(), Player.human)
	}
	
	/* test this situation
	 o - x
	 o x -
	 x - o
	*/
	func testIfGameDetectsPlayerWonWithLeftDiagonalCombination(){
		//Assert that we have no winner in the beginning
		XCTAssertNil(game.getWinner())
		
		let selectionSequence = [0,2,3,4,8,6]
		setPlayersSelections(playersSelectionsSequence: selectionSequence)
		
		//We expect the cross to win, in this case, the human player
		XCTAssertEqual(game.getWinner(), Player.aI)
	}
	
	func testIfGameDoNotAllowSelectionInOccupiedSquares(){
		//Assert that the first square is free in the beginning of the game
		XCTAssertEqual(game.getPositionPlayer(index: 0), nil)
		//AI plays first, so we select the first spot
		game.setCurrentPlayerSelection(index: 0)
		//Assert that the first square is occupied by aI
		XCTAssertEqual(game.getPositionPlayer(index: 0), Player.human)
		//The human player tries to select the same spot
		game.setCurrentPlayerSelection(index: 0)
		//Assert that the square state does not change
		XCTAssertEqual(game.getPositionPlayer(index: 0), Player.human)
	}
	
	/* test this situation
	 o o o
	 x x -
	 - - -
	*/
	func testIfGameHighlightTheCorrectCellsWhenPlayerWins(){
		let selectionSequence = [0,3,1,4,2]
		setPlayersSelections(playersSelectionsSequence: selectionSequence)

		//We expect that all the first line cells are highlighted
		XCTAssertTrue(game.isCellHighlighted(index: 0))
		XCTAssertTrue(game.isCellHighlighted(index: 1))
		XCTAssertTrue(game.isCellHighlighted(index: 2))
	}
	
	/* test this situation
	 o o o
	 x x -
	 - - -
	*/
	func testIfGameUpdatesAIScore(){
		let gameSubscriber = MockGameSubscriber()
		
		let expect = expectation(description: "Test should return winner as Human and score AI:0 and Human:1")
		gameSubscriber.asyncExpectation = expect

		let selectionSequence = [0,3,1,4,2]
		for selection in selectionSequence{
			gameSubscriber.game!.setCurrentPlayerSelection(index: selection)
		}
		
		waitForExpectations(timeout: 1) { error in
			XCTAssertEqual(gameSubscriber.circleScore, "1")
			XCTAssertEqual(gameSubscriber.crossScore, "0")
			XCTAssertEqual(gameSubscriber.winner, Player.human)
			gameSubscriber.asyncExpectation = nil
		}
	}
	
	/* test this situation
	 o x o
	 o x o
	 x o x
	*/
	func testIfGameDetectsDraw(){
		let gameSubscriber = MockGameSubscriber()
	
		let expect = expectation(description: "Test should return winner as nil")
		gameSubscriber.asyncExpectation = expect
		
		let selectionSequence = [0,1,3,6,2,4,7,8,5]
		for selection in selectionSequence{
			gameSubscriber.game!.setCurrentPlayerSelection(index: selection)
		}
		
		waitForExpectations(timeout: 1) { error in
			XCTAssertEqual(gameSubscriber.circleScore, "0")
			XCTAssertEqual(gameSubscriber.crossScore, "0")
			XCTAssertEqual(gameSubscriber.winner, nil)
			gameSubscriber.asyncExpectation = nil
		}
	}
	
	private func setPlayersSelections(playersSelectionsSequence: [Int]){
		for selection in playersSelectionsSequence{
			game.setCurrentPlayerSelection(index: selection)
		}
	}

}
