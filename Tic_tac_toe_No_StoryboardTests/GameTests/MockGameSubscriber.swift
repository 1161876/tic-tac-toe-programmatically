//
//  SpyGameDelegate.swift
//  Tic_tac_toe_No_StoryboardTests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class MockGameSubscriber: GameSubscriber{
	
	var game: Game?
	var winner:Player? = nil
	var crossScore = "0"
	var circleScore = "0"
	var asyncExpectation: XCTestExpectation?
	
	init() {
		game = Game(subscriber: self)
	}
	
	func didFoundWinner(winner: Player?) {
		guard let expectation = asyncExpectation else {
			return
		}
		
		self.winner = winner
		expectation.fulfill()
	}
	
	func didUpdateScore(crossScore: String, circleScore: String) {
		guard asyncExpectation != nil else {
			return
		}
		
		self.crossScore = crossScore
		self.circleScore = circleScore
	}
}
