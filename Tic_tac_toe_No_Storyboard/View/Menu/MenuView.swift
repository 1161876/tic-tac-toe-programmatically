
import UIKit

class MenuView: UIView{
	
	let bottomButton = MenuButton()
	let topButton = MenuButton()
	private let logoImage = UIImageView()
	
	private let sidePadding:CGFloat = 20.0
	private var viewHeight:CGFloat!
	private var viewWidth:CGFloat!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupSubviews()
		self.accessibilityIdentifier = "menuView"
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setButtonsText(topButtonText: String, bottomButtonText: String){
		topButton.setText(buttonText: topButtonText)
		bottomButton.setText(buttonText: bottomButtonText)
	}
	
	private func setupSubviews(){
		viewHeight = super.bounds.size.height
		viewWidth = super.bounds.size.width
		backgroundColor = Color.red
		setupBottomButton()
		setupTopButton()
		setupLogoImage()
	}
	
	private func setupBottomButton(){
		bottomButton.setText(buttonText: "Solo")
		
		self.addSubview(bottomButton)
		
		bottomButton.translatesAutoresizingMaskIntoConstraints 													= false
		bottomButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -viewHeight*0.15).isActive 		= true
		bottomButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: sidePadding).isActive			= true
		bottomButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -sidePadding).isActive 		= true
		bottomButton.heightAnchor.constraint(equalToConstant: viewHeight*0.1).isActive 							= true
	}
	
	
	private func setupTopButton(){
		topButton.setText(buttonText: "1v1")
		
		self.addSubview(topButton)
		
		topButton.translatesAutoresizingMaskIntoConstraints 													= false
		topButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: sidePadding).isActive 				= true
		topButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -sidePadding).isActive			= true
		topButton.heightAnchor.constraint(equalToConstant: viewHeight*0.1).isActive								= true
		topButton.bottomAnchor.constraint(equalTo: bottomButton.topAnchor, constant: -30).isActive 				= true
	}
	
	private func setupLogoImage(){
		self.addSubview(logoImage)
		logoImage.image = Images.appIcon
		
		logoImage.translatesAutoresizingMaskIntoConstraints 													= false
		logoImage.heightAnchor.constraint(equalToConstant: viewWidth*0.6).isActive								= true
		logoImage.widthAnchor.constraint(equalToConstant: viewWidth*0.6).isActive								= true
		logoImage.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive						= true
		logoImage.bottomAnchor.constraint(equalTo: topButton.topAnchor, constant: -viewHeight*0.1).isActive		= true
	}
}
