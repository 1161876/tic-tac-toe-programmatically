
import UIKit

class MenuButton: UIButton{
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupButtonStyle()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupButtonStyle(){
		self.setTitleColor(.white, for: .normal)
		self.titleLabel?.textAlignment = .center
		self.titleLabel?.adjustsFontSizeToFitWidth = true
		self.titleLabel?.minimumScaleFactor = 0.5
		self.backgroundColor = .clear
		self.layer.cornerRadius = 5
		self.layer.borderColor = UIColor.white.cgColor
		self.layer.borderWidth = 3
		self.titleLabel!.font =  UIFont(name: Fonts.bold, size: 30)
		
	}
	
	func setText(buttonText: String){
		self.setTitle(buttonText, for: .normal)
	}
}
