
import UIKit

class PlayerImage{
	class func get(player: Player, isDarkMode: Bool) -> UIImage{
		if player == Player.aI{
			if isDarkMode{
				return Images.darkModeCross
			}
			return Images.lightModeCross
		}else{
			if isDarkMode{
				return Images.darkModeCircle
			}
			return Images.lightModeCircle
		}
	}
}
