import UIKit

extension UINavigationItem{
	
	func showOnlyBackButton(){
		self.rightBarButtonItem = nil
		self.title = nil
		setupBackButton()
	}
	
	func showAllButtons(titleText: String){
		setupBackButton()
		setupModeButton()
		self.title = titleText
	}
	
	func hide(){
		self.leftBarButtonItem = nil
		self.rightBarButtonItem = nil
		self.title = ""
	}
	
	private func setupBackButton(){
		let backButton = BackButton(frame: .infinite, isDarkMode: true)
		backButton.accessibilityIdentifier = "NavigationBarBackButton"
		backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
		self.leftBarButtonItem = UIBarButtonItem(customView: backButton)
	}

	private func setupModeButton(){
		let modeButton = ModeButton(frame: .infinite, isDarkMode: true)
		modeButton.accessibilityIdentifier = "NavigationBarModeButton"
		modeButton.addTarget(self, action: #selector(modeButtonPressed), for: .touchUpInside)
		self.rightBarButtonItem = UIBarButtonItem(customView: modeButton)
	}
	
	@objc func backButtonPressed(){
		NotificationCenter.default.post(name: NotificationName.backButtonPressed, object: nil)
	}
	
	@objc func modeButtonPressed(){
		NotificationCenter.default.post(name: NotificationName.modeButtonPressed, object: nil)
	}
}
