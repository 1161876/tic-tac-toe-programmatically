import UIKit

extension UINavigationController{
	
	func setupColorMode(isDarkMode: Bool){
		var backgroundColor = Color.white
		var navigationBarColor = Color.darkGray
		
		if isDarkMode{
			backgroundColor = Color.darkGray
			navigationBarColor = Color.white
		}
		
		self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: navigationBarColor]
		self.navigationBar.tintColor = navigationBarColor
		self.view.backgroundColor = backgroundColor
	}
	
	func setupStyle(){
		self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
		self.navigationBar.shadowImage = UIImage()
		self.navigationBar.isTranslucent = true
		self.view.backgroundColor = UIColor.clear
	}
}
