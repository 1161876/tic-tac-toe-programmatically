
import UIKit

class BottomGameView: UIView, UIMode{
	
	private var scoreStack = UIStackView()
	private var mainStack = UIStackView()
	var newGameButton: UIButton!
	private var crossPlayerScore: ScoreIndicatorView!
	private var circlePlayerScore: ScoreIndicatorView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupNewGameButton()
		setupScoreIndicators()
		setupScoreAndButtonStack()
		setAccessibilitiesIdentifiers()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setAccessibilitiesIdentifiers(){
		crossPlayerScore.playerScore.accessibilityIdentifier 		= "gameCrossScore"
		circlePlayerScore.playerScore.accessibilityIdentifier		= "gameCircleScore"
	}
	
	func updateScore(crossScore: String, circleScore: String){
		crossPlayerScore.updateScore(score: crossScore)
		circlePlayerScore.updateScore(score: circleScore)
	}
	
	func setMode(isDarkMode: Bool) {
		if isDarkMode{
			self.backgroundColor = Color.darkGray
			self.newGameButton.setTitleColor(Color.white, for: .normal)
		}else{
			self.backgroundColor = Color.white
			self.newGameButton.setTitleColor(Color.darkGray, for: .normal)
		}
		crossPlayerScore.setMode(isDarkMode: isDarkMode, player: Player.aI)
		circlePlayerScore.setMode(isDarkMode: isDarkMode, player: Player.human)
	}
	
	private func setupNewGameButton(){
		newGameButton = UIButton(frame: frame)

		newGameButton.titleLabel?.textAlignment = .center
		newGameButton.titleLabel?.adjustsFontSizeToFitWidth = true
		newGameButton.setTitle("New Game", for: .normal)
		newGameButton.setTitleColor(.black, for: .normal)
		newGameButton.titleLabel?.font = UIFont(name: Fonts.regular, size: 25)
	}
	
	private func setupScoreIndicators(){
		crossPlayerScore = ScoreIndicatorView(frame: self.frame)
		crossPlayerScore.updateImage(image: Images.lightModeCross)
		circlePlayerScore = ScoreIndicatorView(frame: self.frame)
		circlePlayerScore.updateImage(image: Images.lightModeCircle)
		
		let scoreStackViews = [circlePlayerScore!, crossPlayerScore!]
		scoreStack = UIStackView(arrangedSubviews: scoreStackViews)
		scoreStack.distribution = .equalSpacing
		scoreStack.axis = .horizontal
		scoreStack.alignment = .center
	}
	
	private func setupScoreAndButtonStack(){
		let mainStackViews = [scoreStack, newGameButton!]
		mainStack = UIStackView(arrangedSubviews: mainStackViews)
		mainStack.distribution = .fillEqually
		mainStack.alignment = .center
		mainStack.axis = .vertical
		
		self.addSubview(mainStack)
		mainStack.pin(to: self, padding: self.frame.height*0.1)
	}
}
