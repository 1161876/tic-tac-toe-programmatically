
import UIKit

class ScoreIndicatorView: UIView{
	
	private let playerImage = UIImageView()
	var playerScore = UILabel()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = .clear
		setupPlayerImage()
		setupPlayerScore()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setMode(isDarkMode: Bool, player: Player) {
		playerImage.image = PlayerImage.get(player: player, isDarkMode: isDarkMode)
		if isDarkMode{
			self.playerScore.textColor = Color.white
		}else{
			self.playerScore.textColor = Color.darkGray
		}
	}

	func updateScore(score: String){
		playerScore.text = score
	}
	
	func updateImage(image: UIImage){
		self.playerImage.image = image
	}
	
	private func setupPlayerImage(){
		self.addSubview(playerImage)
		playerImage.translatesAutoresizingMaskIntoConstraints 										= false
		playerImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive 	= true
		playerImage.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive		= true
		playerImage.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive			= true
		playerImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive					= true
	}
	
	private func setupPlayerScore(){
		playerScore.text = "0"
		playerScore.adjustsFontSizeToFitWidth = true
		playerScore.minimumScaleFactor = 0.5
		playerScore.font =  UIFont(name: Fonts.regular, size: 25)
		playerScore.textColor = .black
		playerScore.textAlignment = .center
		
		self.addSubview(playerScore)
		
		playerScore.translatesAutoresizingMaskIntoConstraints 													= false
		playerScore.leadingAnchor.constraint(equalTo: self.playerImage.trailingAnchor, constant: 5).isActive 	= true
		playerScore.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive 								= true
		playerScore.topAnchor.constraint(equalTo: self.topAnchor).isActive 										= true
		playerScore.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive				= true
	}
	
}
