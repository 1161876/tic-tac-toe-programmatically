
import UIKit
import NVActivityIndicatorView

class TopView: UIView, UIMode{

	private var topViewLabel = UILabel()
	private var topViewPlayerImage = UIImageView()
	let loadingIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .ballPulse, color: Color.darkGray, padding: 0)
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = .clear
		setupViews(labelText: "", playerImage: nil)
		setAccessibilitiesIdentifiers()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setAccessibilitiesIdentifiers(){
		topViewLabel.accessibilityIdentifier 		= "gameWinnerLabel"
		topViewPlayerImage.accessibilityIdentifier	= "gameWinnerImage"
	}
	
	func setupViews(labelText: String, playerImage: UIImage?){
		topViewLabel.isHidden = false
		topViewPlayerImage.isHidden = false
		setupLoadingIndicator()
		setupLabel(labelText: labelText)
		if playerImage == nil{
			topViewPlayerImage.image = UIImage()
			setupLabelConstraints()
			return
		}
		setupPlayerImage(playerImage: playerImage!)
		setupMainStackView()
	}
	
	private func setupLoadingIndicator (){
		self.addSubview(loadingIndicator)
		
		loadingIndicator.translatesAutoresizingMaskIntoConstraints 									= false
		loadingIndicator.widthAnchor.constraint(equalToConstant: 50).isActive						= true
		loadingIndicator.heightAnchor.constraint(equalToConstant: 50).isActive						= true
		loadingIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive				= true
		loadingIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive				= true
		
	}
	
	func setMode(isDarkMode: Bool) {
		if isDarkMode{
			self.topViewLabel.textColor = Color.white
			self.loadingIndicator.color = Color.white
		}else{
			self.topViewLabel.textColor = Color.darkGray
			self.loadingIndicator.color = Color.darkGray
		}
		self.topViewPlayerImage.image = self.topViewPlayerImage.image!.changeToOppositeMode()
	}
	
	func hideViews(){
		topViewLabel.isHidden = true
		topViewPlayerImage.isHidden = true
	}
	
	private func setupLabel(labelText: String){
		topViewLabel.text = labelText
		topViewLabel.baselineAdjustment = .alignCenters
		topViewLabel.textAlignment = .center
		topViewLabel.adjustsFontSizeToFitWidth = true
		topViewLabel.minimumScaleFactor = 0.5
		topViewLabel.font =  UIFont(name: Fonts.regular, size: 25)
	}
	
	private func setupLabelConstraints(){
		self.addSubview(topViewLabel)
		
		topViewLabel.translatesAutoresizingMaskIntoConstraints 													= false
		topViewLabel.widthAnchor.constraint(greaterThanOrEqualTo: self.widthAnchor, multiplier: 0.6).isActive	= true
		topViewLabel.heightAnchor.constraint(equalToConstant: 50).isActive										= true
		topViewLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive								= true
		topViewLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive								= true
	}
	
	private func setupPlayerImage(playerImage: UIImage){
		topViewPlayerImage.image = playerImage
		topViewPlayerImage.contentMode = .scaleAspectFit
	}
	
	private func setupMainStackView(){
		
		let stackViewItems = [topViewLabel, topViewPlayerImage]
		let stackView = UIStackView(arrangedSubviews: stackViewItems)
		stackView.axis = .horizontal
		stackView.distribution = .fillProportionally
		stackView.alignment = .center
		
		self.addSubview(stackView)
		setupMainSackViewConstraints(stackView: stackView)
	}
	
	private func setupMainSackViewConstraints(stackView: UIStackView){
		stackView.translatesAutoresizingMaskIntoConstraints 									= false
		stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive				= true
		stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive				= true
		stackView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.6).isActive 	= true
		stackView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3).isActive = true
	}
}
