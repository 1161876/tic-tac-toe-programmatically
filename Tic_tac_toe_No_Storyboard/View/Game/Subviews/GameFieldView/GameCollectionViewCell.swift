
import UIKit

class GameCollectionViewCell: UICollectionViewCell {
	
	private let cellImage = UIImageView()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupSubviews(){
		self.addSubview(cellImage)
		cellImage.pin(to: self, padding: 12)
	}
	
	func setCellImage(image: UIImage){
		cellImage.image = image
	}
	
	func setCellBackgroundColor(isHighLighted: Bool, isDarkMode: Bool){
		if !isHighLighted{
			self.backgroundColor = .clear
			return
		}
		if isDarkMode{
			self.backgroundColor = Color.darkGreen
		}else{
			self.backgroundColor = Color.lightGreen
		}
	}
}
