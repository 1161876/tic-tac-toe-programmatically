
import UIKit

class GameFieldView: UIView, UIMode{
	
	private let gameFieldImage = UIImageView()
	var boardCollectionView: UICollectionView!

	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = .white
		setupSubviews()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setMode(isDarkMode: Bool) {
		if isDarkMode{
			self.backgroundColor = Color.darkGray
		}else{
			self.backgroundColor = Color.white
		}
	}

	private func setupSubviews(){
		setupBoardCollectionView()
		setupBoardImage()
	}
	
	private func setupBoardImage(){
		gameFieldImage.image = Images.gameBoard
		gameFieldImage.tintColor = Color.lightGray
		addSubview(gameFieldImage)
		gameFieldImage.pin(to: self, padding: 5)
	}
	
	private func setupBoardCollectionView(){
		self.boardCollectionView = UICollectionView(frame: frame, collectionViewLayout: .init())
		boardCollectionView.backgroundColor = .clear
		boardCollectionView.register(GameCollectionViewCell.self, forCellWithReuseIdentifier: CellIdentifier.gameCollection)
		boardCollectionView.isScrollEnabled = false
		layoutCells()
		addSubview(boardCollectionView!)
		boardCollectionView.pin(to: self, padding: 5)
	}

	private func layoutCells() {
		let layout = UICollectionViewFlowLayout()
		let viewWidth = self.bounds.size.width
		layout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 0, right: 2)
		layout.minimumInteritemSpacing = 10
		layout.minimumLineSpacing = 10
		layout.itemSize = CGSize(width: (viewWidth - 35)/3, height: ((viewWidth - 35)/3))
		boardCollectionView.collectionViewLayout = layout
	}
	
}
