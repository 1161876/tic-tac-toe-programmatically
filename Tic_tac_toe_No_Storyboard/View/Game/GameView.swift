
import UIKit

class GameView: UIView{
	
	private var viewHeight:CGFloat!
	private var viewWidth:CGFloat!
	var gameFieldCollectionView: UICollectionView!
	private var gameFieldView: GameFieldView!
	let bottomView = BottomGameView()
	private let topView = TopView()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.accessibilityIdentifier = "gameView"
		viewHeight = frame.size.height
		viewWidth =  frame.size.width
		setupBoardView()
		setupBottomView()
		setNewGame()
		setupTopView()
		stopLoadingIndicator()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func updateWinner(winner: Player?, isDarkMode: Bool){
		if winner == nil{
			topView.setupViews(labelText: "It's a Draw", playerImage: nil)
		}else if winner == Player.aI{
			topView.setupViews(labelText: "The Winner Is:", playerImage: PlayerImage.get(player: winner!, isDarkMode: isDarkMode))
		}else{
			topView.setupViews(labelText: "The Winner Is:", playerImage: PlayerImage.get(player: winner!, isDarkMode: isDarkMode))
		}
	}
	
	func startLoadingIndicator(){
		topView.loadingIndicator.startAnimating()
	}
	
	func stopLoadingIndicator(){
		topView.loadingIndicator.stopAnimating()
	}
	
	func setMode(isDarkMode: Bool){
		topView.setMode(isDarkMode: isDarkMode)
		gameFieldView.setMode(isDarkMode: isDarkMode)
		bottomView.setMode(isDarkMode: isDarkMode)
		gameFieldCollectionView.reloadData()
	}
	
	func setNewGame(){
		topView.setupViews(labelText: "", playerImage: nil)
		gameFieldCollectionView.reloadData()
	}
	
	private func setupBoardView(){
		gameFieldView = GameFieldView(frame: self.frame)
		gameFieldCollectionView = gameFieldView.boardCollectionView
		
		self.addSubview(gameFieldView)
		
		gameFieldView.translatesAutoresizingMaskIntoConstraints 											= false
		gameFieldView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive 						= true
		gameFieldView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive 						= true
		gameFieldView.heightAnchor.constraint(equalToConstant: min(viewHeight,viewWidth)).isActive			= true
		gameFieldView.widthAnchor.constraint(equalToConstant: min(viewHeight,viewWidth)).isActive			= true
	}
	
	private func setupBottomView(){
		self.addSubview(bottomView)
		
		bottomView.translatesAutoresizingMaskIntoConstraints 												= false
		bottomView.topAnchor.constraint(equalTo: self.gameFieldView.bottomAnchor, constant: 20).isActive	= true
		bottomView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -viewHeight*0.05).isActive = true
		bottomView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive 							= true
		bottomView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive 						= true
	}
	
	private func setupTopView(){
		self.addSubview(topView)
		
		topView.translatesAutoresizingMaskIntoConstraints 													= false
		topView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive 						= true
		topView.bottomAnchor.constraint(equalTo: self.gameFieldView.topAnchor, constant: 20).isActive 		= true
		topView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive 								= true
		topView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive 							= true
	}
	
	func showYourTurn(isYourTurn: Bool){
		if isYourTurn{
			topView.setupViews(labelText: "It's Your Turn", playerImage: nil)
			return
		}
		topView.hideViews()
	}
}

