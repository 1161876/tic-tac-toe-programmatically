
import Foundation

protocol UIMode {
	func setMode(isDarkMode: Bool)
}
