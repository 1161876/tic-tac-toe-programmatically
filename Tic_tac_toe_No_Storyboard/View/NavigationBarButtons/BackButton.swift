
import UIKit

class BackButton: UIButton {

	private var mainColor = UIColor.black
	
	init(frame: CGRect, isDarkMode: Bool) {
		super.init(frame: frame)
		if isDarkMode{ mainColor = UIColor.white }
		setupButton()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupButton(){
		self.setImage(Images.leftArrow, for: .normal)
		self.translatesAutoresizingMaskIntoConstraints				= false
		self.heightAnchor.constraint(equalToConstant: 35).isActive 	= true
		self.widthAnchor.constraint(equalToConstant: 35).isActive 	= true
	}
}
