
import UIKit

class ModeButton: UIButton {

	init(frame: CGRect, isDarkMode: Bool) {
		super.init(frame: frame)
		setupButton(isDarkMode: isDarkMode)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupButton(isDarkMode: Bool){
		var imageHight:CGFloat = 30
		
		if isDarkMode{
			self.setImage(Images.lightModeIcon, for: .normal)
		}else{
			self.setImage(Images.darkModeIcon, for: .normal)
			imageHight = 25
		}

		self.translatesAutoresizingMaskIntoConstraints						= false
		self.heightAnchor.constraint(equalToConstant: imageHight).isActive 	= true
		self.widthAnchor.constraint(equalToConstant: imageHight).isActive 	= true
	}

}
