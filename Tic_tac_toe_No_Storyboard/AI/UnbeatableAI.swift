
class UnbeatableAI : AI {

	func getNextMove(gameField: GameField) -> Int{
		
		var bestMove:Int = 0
		let freeSpots = gameField.getFreeSpots()
		var bestScore = -50
		
		if freeSpots.count == 9{
			//select random position
			return Int.random(in: 0...8)
		}else{
			//find the best position to maximise AI winning chances
			for n in 0...freeSpots.count - 1{
				let tempGameField  = GameField()
				tempGameField.setField(field: gameField.getField())
				tempGameField.setPlayerSpot(index: freeSpots[n], player: Player.aI)
				
				let score = self.minMax(gameField: tempGameField, isMinimizing: true, level: 0)
				
				if score > bestScore{
					bestScore = score
					bestMove = freeSpots[n]
				}
			}
			
			return bestMove
		}
	}
	
	private func minMax(gameField: GameField, isMinimizing: Bool, level: Int) -> Int{
		let result = getScoreForField(gameField: gameField)
		
		//check if we reach the end of the tree
		if result != nil{
			return result!
		}

		let availablePositions = gameField.getFreeSpots()
		var bestScore = -50
		
		if isMinimizing{
			bestScore = 50
		}
		
		//find the best position to maximise AI winning chances
		for n in 0...availablePositions.count - 1{

			let tempGameField  = GameField()
			tempGameField.setField(field: gameField.getField())
			
			var player = Player.aI
			if level%2 == 0{
				player = Player.human
			}
			
			tempGameField.setPlayerSpot(index: availablePositions[n],player: player)
			
			if isMinimizing{
				let score = minMax(gameField: tempGameField, isMinimizing: false, level: level + 1)
				bestScore = min(bestScore,score)
			}else{
				let score = minMax(gameField: tempGameField, isMinimizing: true, level: level + 1)
				bestScore = max(bestScore,score)
			}
		}
		
		return bestScore
	}
	
	private func getScoreForField(gameField: GameField) -> Int?{
		if gameField.doesPlayerWon(player: Player.aI){
			return 1
		}
		if gameField.doesPlayerWon(player: Player.human){
			return -1
		}
		if gameField.getFreeSpots().count == 0{
			return 0
		}
		return nil
	}

}
