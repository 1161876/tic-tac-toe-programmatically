
protocol AI {
	 func getNextMove(gameField: GameField) -> Int
}
