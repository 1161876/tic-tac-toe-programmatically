
class ModerateAI : AI{
	
	func getNextMove(gameField: GameField) -> Int{
		
		let freeSpots = gameField.getFreeSpots()
		
		//check if AI can win
		if freeSpots.count < 6{
			if let nextMoveToWin = nextMove(gameField: gameField, player: Player.aI){
				return nextMoveToWin
			}
		}
		
		//Check if AI should defend
		if freeSpots.count <= 6{
			if let nextMoveToDefend = nextMove(gameField: gameField, player: Player.human){
				return nextMoveToDefend
			}
		}
		
		//send random value in a free spot
		return getRandomMove(gameField: gameField)
	}
	
	private func getRandomMove(gameField: GameField) -> Int{
		
		var index = Int.random(in: 0...8)
		var isFree = gameField.isSpotFree(index: index)
		
		while !isFree{
			index = Int.random(in: 0...8)
			isFree = gameField.isSpotFree(index: index)
		}
		
		return index
	}
	
	
	private func nextMove(gameField: GameField, player: Player) -> Int?{
		
		//check if we can win in a line
		for i in 0...2{
			if gameField.foundHitsInLine(i: i, player: player, numberOfHits: 2){
				if let nextMove = gameField.getFreeSpotInLine(i: i){
					return nextMove
				}
			}
		}
		
		//check if we can win in a column
		for j in 0...2{
			if gameField.foundHitsInColumn(j: j, player: player, numberOfHits: 2){
				if let nextMove = gameField.getFreeSpotInColumn(j: j){
					return nextMove
				}
			}
		}
		
		//check if we can win in right diagonal
		if gameField.foundHitsInRightDiagonal(player: player, numberOfHits: 2){
			if let nextMove = gameField.getFreeSpotInRightDiagonal(){
				return nextMove
			}
		}
		
		//check if we can win in left diagonal
		if gameField.foundHitsInLeftDiagonal(player: player, numberOfHits: 2){
			if let nextMove = gameField.getFreeSpotInLeftDiagonal(){
				return nextMove
			}
		}
		
		return nil
	}
}
