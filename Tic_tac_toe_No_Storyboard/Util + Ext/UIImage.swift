
import UIKit

extension UIImage{
	func changeToOppositeMode()-> UIImage{
		switch self {
		case Images.darkModeCircle:
			return Images.lightModeCircle
		case Images.lightModeCircle:
			return Images.darkModeCircle
		case Images.darkModeCross:
			return Images.lightModeCross
		case Images.lightModeCross:
			return Images.darkModeCross
		default:
			return self
		}
	}
}
