
import Foundation

class Settings {
	
	class func isDarkMode() -> Bool{
		let isDarkMode = UserDefaults.standard.bool(forKey: "isDarkMode")
		if isDarkMode == true{
			return true
		}else{
			return false
		}
	}
	
	class func setMode(isDarkMode: Bool){
		UserDefaults.standard.set(isDarkMode, forKey: "isDarkMode")
	}
}
