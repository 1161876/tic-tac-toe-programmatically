
import UIKit

struct Images{
	//Image constants assets
	static let appIcon			= UIImage(named:"icon")!
	static let darkModeIcon		= UIImage(named:"dark")!
	static let lightModeIcon	= UIImage(named:"lightMode")!
	static let leftArrow		= UIImage(named:"leftArrow")!
	static let gameBoard		= UIImage(named:"tic_tac_toe_board")!
	static let lightModeCircle	= UIImage(named:"light_circle")!
	static let darkModeCircle	= UIImage(named:"darkModeCircle")!
	static let lightModeCross	= UIImage(named:"lightModeCross")!
	static let darkModeCross	= UIImage(named:"darkModeCross")!
}

struct Color {
	//Color constants
	static let red				= UIColor(red: 224/255, green: 24/255, blue: 41/255, alpha: 1)
	static let white			= UIColor.white
	static let darkGray			= UIColor.init(red: 61/255, green: 56/255, blue: 56/255, alpha: 1)
	static let lightGray		= UIColor.init(red: 125/255, green: 118/255, blue: 119/255, alpha: 1)
	static let black			= UIColor.black
	static let lightGreen		= UIColor.init(red: 226/255, green: 240/255, blue: 217/255, alpha: 1)
	static let darkGreen		= UIColor.init(red: 50/255, green: 77/255, blue: 35/255, alpha: 1)
}

struct CellIdentifier {
	static let gameCollection	= "gameCollectionViewCell"
}

struct NotificationName{
	static let backButtonPressed = Notification.Name("tiagosantos.ios.backButtonPressed")
	static let modeButtonPressed = Notification.Name("tiagosantos.ios.modeButtonPressed")
}

struct Fonts{
	static let regular	= "Montserrat-Regular"
	static let bold		= "Montserrat-Bold"
}
