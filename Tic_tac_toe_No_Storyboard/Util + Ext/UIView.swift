
import UIKit

extension UIView{
	
	func pin(to superView: UIView, padding: CGFloat){
		translatesAutoresizingMaskIntoConstraints 													= false
		topAnchor.constraint(equalTo: superView.topAnchor, constant: padding).isActive				= true
		leadingAnchor.constraint(equalTo: superView.leadingAnchor, constant: padding).isActive		= true
		trailingAnchor.constraint(equalTo: superView.trailingAnchor, constant: -padding).isActive	= true
		bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -padding).isActive		= true
	}
	
}
