
import UIKit

class GameViewController: UIViewController, GameSubscriber {
	
	private var game: Game!
	var gameView: GameView!
	var isDarkMode = false
	private var isGameInProgress = true
	private var isAITurn = false
	var gameMode = GameMode.solo
	
	override func viewDidLoad() {
		super.viewDidLoad()
		game = Game(subscriber: self)
		isDarkMode = Settings.isDarkMode()
		setupMenuView()
		setupNavigationItems()
		setupModeColor()
		showYourTurnIfNeeded()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.title = gameMode.rawValue
	}
	
	override func viewDidAppear(_ animated: Bool) {
		addNavigationBarObservers()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		NotificationCenter.default.removeObserver(self)
	}
	
	private func addNavigationBarObservers(){
		NotificationCenter.default.addObserver(self, selector: #selector(backButtonPressed), name: NotificationName.backButtonPressed, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(modeButtonPressed), name: NotificationName.modeButtonPressed, object: nil)
	}
	
	private func setupMenuView(){
		//setup game view
		gameView = GameView(frame: view.frame)
		gameView.gameFieldCollectionView.dataSource = self
		gameView.gameFieldCollectionView.delegate = self
		gameView.setMode(isDarkMode: isDarkMode)
		
		//setup game view constraints
		view.addSubview(gameView)
		gameView.pin(to: view, padding: 0)
		
		//setup gameView button action
		gameView.bottomView.newGameButton.addTarget(self, action: #selector(newGameButtonPressed), for: .touchUpInside)
	}
	
	private func setupNavigationItems(){
		self.navigationItem.showAllButtons(titleText: self.gameMode.rawValue)
	}
	
	@objc func backButtonPressed(){
		self.navigationController?.popViewController(animated: false)
	}
	
	@objc func modeButtonPressed(){
		isDarkMode = !isDarkMode
		Settings.setMode(isDarkMode: isDarkMode)
		setupNavigationItems()
		setupModeColor()
		self.gameView.setMode(isDarkMode: isDarkMode)
	}
	
	@objc func newGameButtonPressed(){
		isGameInProgress = true
		game.newGame()
		gameView.setNewGame()
		checkAITurn()
	}
	
	private func setupModeColor(){
		self.navigationController?.setupColorMode(isDarkMode: self.isDarkMode)
	}
	
	func didFoundWinner(winner: Player?) {
		gameView.updateWinner(winner: winner, isDarkMode: isDarkMode)
		isGameInProgress = false
	}
	
	func didUpdateScore(crossScore: String, circleScore: String) {
		isGameInProgress = false
		gameView.bottomView.updateScore(crossScore: crossScore, circleScore: circleScore )
	}
	
	//This method is used to filter double selection
	private func setNoSelectionTime(){
		gameView.gameFieldCollectionView?.allowsSelection = false
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
			//self.aIIndicatorView.stopAnimating()
			self.gameView.gameFieldCollectionView?.allowsSelection = true
		}
	}
	
	private func checkAITurn(){
		if isGameInProgress{
			showYourTurnIfNeeded()
		}
		if gameMode == GameMode.solo || !game.isAIMove() || !isGameInProgress{
			return
		}
		gameView.startLoadingIndicator()
		gameView.gameFieldCollectionView?.allowsSelection = false
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
			let aIMove = self.getAIMove()
			self.gameView.gameFieldCollectionView!.allowsSelection = true
			self.gameView.gameFieldCollectionView!.selectItem(at: IndexPath(item: aIMove, section: 0), animated: true, scrollPosition: .bottom)
			self.collectionView(self.gameView.gameFieldCollectionView!, didSelectItemAt: IndexPath(item: aIMove, section: 0))
		}
	}
	
	private func showYourTurnIfNeeded(){
		if !game.isAIMove() && gameMode != GameMode.solo && game.getWinner() == nil{
			gameView.showYourTurn(isYourTurn: true)
			return
		}
		gameView.showYourTurn(isYourTurn: false)
	}
	
	private func getAIMove() -> Int{
		if gameMode == GameMode.impossible{
			return UnbeatableAI().getNextMove(gameField: self.game.getGameField())
		}
		return ModerateAI().getNextMove(gameField: self.game.getGameField())
	}
	
}

extension GameViewController: UICollectionViewDelegate, UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 9
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.gameCollection, for: indexPath) as! GameCollectionViewCell
		let player = game.getPositionPlayer(index: indexPath.row)
		if player == nil{
			cell.setCellImage(image: UIImage())
		}else{
			cell.setCellImage(image: PlayerImage.get(player: player!, isDarkMode: isDarkMode))
		}
		cell.setCellBackgroundColor(isHighLighted: game.isCellHighlighted(index: indexPath.row), isDarkMode: isDarkMode)
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		gameView.stopLoadingIndicator()
		if !isGameInProgress {return}
		setNoSelectionTime()
		game.setCurrentPlayerSelection(index: indexPath.row)
		gameView.gameFieldCollectionView?.reloadData()
		checkAITurn()
	}
}
