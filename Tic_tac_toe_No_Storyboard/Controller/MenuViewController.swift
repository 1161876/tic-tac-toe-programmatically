
import UIKit

class MenuViewController: UIViewController {
	
	private var menuView = MenuView()
	private var selectedGameMode = GameMode.solo
	private var isGameModeSelected = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupMenuView()
	}
	override func viewWillAppear(_ animated: Bool) {
		hideNavigationBar()
		isGameModeSelected = false
		setModeSelection()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		NotificationCenter.default.addObserver(self, selector: #selector(backButtonPressed), name: NotificationName.backButtonPressed, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		NotificationCenter.default.removeObserver(self)
	}
	
	private func setupMenuView(){
		menuView = MenuView(frame: view.frame)
		view.addSubview(menuView)
		menuView.pin(to: view, padding: 0)
		
		//Add menu view button targets
		menuView.topButton.addTarget(self, action: #selector(topButtonPressed), for: .touchUpInside)
		menuView.bottomButton.addTarget(self, action: #selector(bottomButtonPressed), for: .touchUpInside)
		
		menuView.topButton.accessibilityIdentifier = "MenuTopButton"
		menuView.bottomButton.accessibilityIdentifier = "MenuBottomButton"

	}
	
	private func setModeSelection(){
		menuView.setButtonsText(topButtonText: "1v1", bottomButtonText: "Solo")
	}
	
	private func setDifficultySelection(){
		menuView.setButtonsText(topButtonText: "Moderate", bottomButtonText: "Impossible")
	}
	
	@objc func topButtonPressed(){
		if isGameModeSelected{
			goToGameView(gameMode: GameMode.moderate)
		}else{
			goToGameView(gameMode: GameMode.solo)
		}
	}
	
	@objc func bottomButtonPressed(){
		if !isGameModeSelected{
			isGameModeSelected = true
			setDifficultySelection()
			showNavigationBar()
		}else{
			goToGameView(gameMode: GameMode.impossible)
		}
	}
	
	private func showNavigationBar(){
		self.navigationItem.showOnlyBackButton()
		self.navigationController?.setupColorMode(isDarkMode: true)
	}
	
	private func hideNavigationBar(){
		self.navigationItem.hide()
	}
	
	@objc func backButtonPressed(){
		isGameModeSelected = false
		hideNavigationBar()
		setModeSelection()
	}
	
	private func goToGameView(gameMode: GameMode){
		let nextScreen = GameViewController()
		nextScreen.gameMode = gameMode
		navigationController?.pushViewController(nextScreen, animated: false)
	}
}
