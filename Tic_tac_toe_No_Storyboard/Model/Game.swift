
class Game{
	
	private var circleScore: Int
	private var crossScore: Int
	private var gameField: GameField
	private var firstPlayer: Player
	private var currentPlayer: Player
	private var moveNumber: Int
	private var winningCombination: [Square]?
	private var winner: Player?
	var subscriber: GameSubscriber!
	
	init(subscriber: GameSubscriber){
		self.subscriber = subscriber
		circleScore = 0
		crossScore = 0
		gameField = GameField()
		moveNumber = 0
		firstPlayer = Player.human
		currentPlayer = Player.human
	}
	
	func newGame(){
		winner = nil
		gameField.cleanGameField()
		moveNumber = 0
		if firstPlayer == Player.human{
			firstPlayer = Player.aI
			currentPlayer = Player.aI
		}else{
			firstPlayer = Player.human
			currentPlayer = Player.human
		}
		gameField.cleanGameField()
	}
	
	func isAIMove() -> Bool{
		if currentPlayer == Player.aI{
			return true
		}
		return false
	}
	
	func getMoveNumber() -> Int{
		return moveNumber
	}
	
	func isCellHighlighted(index: Int) -> Bool{
		return self.gameField.isCellHighlighted(index: index)
	}
	
	func checkWinner(index: Int){
		if self.moveNumber < 4{
			return
		}
		
		let i  = Int((index/3))
		let j = index % 3
		
		if gameField.foundHitsInLine(i: i, player: currentPlayer, numberOfHits: 3) {
			setWinner(player: currentPlayer)
			self.gameField.highlightCells(cells: [i*3, i*3 + 1, i*3 + 2])
			return
		}
		
		if gameField.foundHitsInColumn(j: j, player: currentPlayer, numberOfHits: 3) {
			setWinner(player: currentPlayer)
			self.gameField.highlightCells(cells: [3*0 + j, 3*1 + j, 3*2 + j])
			return
		}
		
		if i == j || i == 2 && j == 0 || i == 0 && j == 2{
			//we need to check diagonals
			if gameField.foundHitsInLeftDiagonal(player: currentPlayer, numberOfHits: 3){
				setWinner(player: currentPlayer)
				self.gameField.highlightCells(cells: [0,4,8])
				return
			}
			
			if  gameField.foundHitsInRightDiagonal(player: currentPlayer, numberOfHits: 3){
				setWinner(player: currentPlayer)
				self.gameField.highlightCells(cells: [2,4,6])
				return
			}
		}
		
		//check if it's a draw
		if self.moveNumber == 9{
			self.subscriber.didFoundWinner(winner: nil)
		}
	}
	
	private func setWinner(player: Player?){
		if let winner = player{
			subscriber.didFoundWinner(winner: winner)
			updateScore(player: winner)
		}
		self.winner = player
	}
	
	func setCurrentPlayerSelection(index: Int){

		if gameField.isSpotFree(index: index){
			self.moveNumber += 1
			
			gameField.setPlayerSpot(index: index, player: currentPlayer)
			checkWinner(index: index)
			
			if currentPlayer == Player.aI{
				currentPlayer = Player.human
			}else{
				currentPlayer = Player.aI
			}
		}
		
	}
	
	func getPositionPlayer(index: Int) -> Player?{
		
		let positionValue = gameField.getPositionValue(index: index)
		
		if positionValue == Player.human.rawValue{
			return Player.human
		} else if positionValue == Player.aI.rawValue{
			return Player.aI
		}
		
		return nil
	}
	
	func updateScore(player: Player){
		if player == Player.aI{
			crossScore += 1
		}else{
			circleScore += 1
		}
		subscriber.didUpdateScore(crossScore: String(crossScore), circleScore: String(circleScore))
	}

	func getGameField() -> GameField{
		return gameField
	}
	
	func getWinner() -> Player?{
		return winner
	}
}
