
protocol GameSubscriber{
	func didFoundWinner(winner: Player?)
	func didUpdateScore(crossScore: String, circleScore: String)
}
