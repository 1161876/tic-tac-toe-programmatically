
class GameField{
	
	private var gameField: [[Square]]
	
	init() {
		gameField = [[Square(index: 0),Square(index: 1),Square(index: 2)],
					 [Square(index: 3),Square(index: 4),Square(index: 5)],
					 [Square(index: 6),Square(index: 7),Square(index: 8)]]
	}
	
	func isSpotFree(index: Int) -> Bool{
		
		if gameField[Int((index/3))][index % 3].getValue() == 0{
			return true
		}
		return false
	}
	
	func setField(field: [[Square]]){
		self.gameField = field
	}
	
	func getFreeSpots() -> [Int]{
		var freeSpots:[Int] = []
		
		for i in 0...2{
			for j in 0...2{
				if self.gameField[i][j].getValue() == 0{
					freeSpots.append(gameField[i][j].getIndex())
				}
			}
		}
		return freeSpots
	}
	
	func isCellHighlighted(index: Int) -> Bool{
		let i  = Int((index/3))
		let j = index % 3
		return gameField[i][j].isHighLighted()
	}
	
	func highlightCells(cells: [Int]){
		for cellIndex in cells{
			let i  = Int((cellIndex/3))
			let j = cellIndex % 3
			gameField[i][j].setToHighlighted()
		}
	}
	
	func setPlayerSpot(index: Int, player: Player){
		let i  = Int((index/3))
		let j = index % 3
		gameField[i][j].setValue(value: player.rawValue)
	}
	
	func doesPlayerWon(player: Player) -> Bool{
		
		for n in 0...2{
			if foundHitsInLine(i: n, player: player, numberOfHits: 3){
				return true
			}
			
			if foundHitsInColumn(j: n, player: player, numberOfHits: 3){
				return true
			}
		}
		
		if foundHitsInLeftDiagonal(player: player, numberOfHits: 3){
			return true
		}
		
		if foundHitsInRightDiagonal(player: player, numberOfHits: 3){
			return true
		}
		
		return false
	}
	
	func foundHitsInColumn(j: Int, player: Player, numberOfHits: Int)->Bool{
		var cont = 0
		
		for i in 0...2{
			if self.gameField[i][j].getValue() == player.rawValue{
				cont += 1
				if cont == numberOfHits{
					return true
				}
			}
		}
		
		return false
	}
	
	func foundHitsInLine(i: Int, player: Player, numberOfHits: Int) -> Bool{
		var cont = 0
		
		for j in 0...2{
			if self.gameField[i][j].getValue() == player.rawValue{
				cont += 1
				if cont == numberOfHits{
					return true
				}
			}
		}
		
		return false
	}
	
	func foundHitsInLeftDiagonal(player: Player, numberOfHits: Int) -> Bool{
		if self.gameField[0][0].getValue() + self.gameField[1][1].getValue() + self.gameField[2][2].getValue() == player.rawValue*numberOfHits{
			return true
		}
		return false
	}
	
	func foundHitsInRightDiagonal(player: Player, numberOfHits: Int) -> Bool{
		if self.gameField[0][2].getValue() + self.gameField[1][1].getValue() + self.gameField[2][0].getValue() == player.rawValue*numberOfHits{
			return true
		}
		return false
	}
	
	func getPositionValue(index: Int) -> Int{
		let i  = Int((index/3))
		let j = index % 3
		return self.gameField[i][j].getValue()
	}
	
	func getField() -> [[Square]]{
		return self.gameField
	}
	
	func getFreeSpotInLine(i: Int) -> Int?{
		for j in 0...2{
			if self.gameField[i][j].getValue() == 0{
				return i*3 + j
			}
		}
		return nil
	}
	
	func getFreeSpotInColumn(j: Int) -> Int?{
		for i in 0...2{
			if self.gameField[i][j].getValue() == 0{
				return i*3 + j
			}
		}
		return nil
	}
	
	func getFreeSpotInLeftDiagonal() -> Int?{
		if self.gameField[0][0].getValue() == 0{
			return 0
		}
		if self.gameField[1][1].getValue() == 0{
			return 4
		}
		if self.gameField[2][2].getValue() == 0{
			return 8
		}
		return nil
	}
	
	func getFreeSpotInRightDiagonal() -> Int?{
		if self.gameField[0][2].getValue() == 0{
			return 2
		}
		if self.gameField[1][1].getValue() == 0{
			return 4
		}
		if self.gameField[2][0].getValue() == 0{
			return 6
		}
		return nil
	}
	
	func cleanGameField(){
		gameField = [[Square(index: 0),Square(index: 1),Square(index: 2)],
					[Square(index: 3),Square(index: 4),Square(index: 5)],
					[Square(index: 6),Square(index: 7),Square(index: 8)]]
	}
}
