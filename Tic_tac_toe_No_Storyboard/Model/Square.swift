
struct Square{
	
	private var index: Int
	private var isHighlighted: Bool
	private var value: Int
	
	init(index: Int) {
		self.index = index
		self.isHighlighted = false
		self.value = 0
	}
	
	func getIndex()-> Int{
		return index
	}
	
	mutating func setValue(value: Int){
		if value != Player.aI.rawValue && value != Player.human.rawValue{
			self.value = 0
		}
		self.value = value
	}
	
	func getValue() -> Int{
		return value
	}
	
	func isHighLighted() -> Bool{
		return isHighlighted
	}
	
	mutating func setToHighlighted(){
		isHighlighted = true
	}
}
