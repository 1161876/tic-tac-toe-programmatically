# Tic-Tac-Toe #

The Tic-Tac-Toe application has an intuitive UI that supports dark mode, has a single player mode with a moderate difficulty that is challenging, an impossible difficulty that is designed to be unbeatable, and supports multi player mode.

## Objective: ##

This application is a personal pet project. It started as a challenge from a friend that evolved to a fun project.
This version of the project was done using UIKit and all the UI was created programmatically.

You can see a version of this same project using Storyboards in this repository:
https://bitbucket.org/1161876/tic-tac-toe-storyboard/src/master/

## Screenshots: ##

![Application screenshots](screenshots.png)

## Game Modes: ##
   - **Impossible** - In this mode, the user plays against a Maxmin algorithm that never loses.
   - **Moderate** - In this mode, the difficulty is average.
   - **Multiplayer Mode** - In this mode, two users can play against each other.

### Project Dependencies:

This project uses the **CocoaPods** following library **NVActivityIndicatorView 4.8.0**

### About Me:

My name is Tiago Santos and I am an iOS developer for more than 2 years and I am extremely motivated to expand my knowledge and embrace a new challenge.
Due to my early contact with the sales business, I was able to develop a solid sense of product which is key in the mobile development process. My main goal is to create innovative and solid applications.

## Contacts: ##
   - +33 (0)7 49 27 87 17
   - ios.tiagosantos@gmail.com
   - www.linkedin.com/in/tiagoandresantos
