//
//  GameTests.swift
//  Tic_tac_toe_No_StoryboardUITests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class GameTests: XCTestCase {
	
	let app =  XCUIApplication()
	let firstButton = XCUIApplication().buttons["MenuTopButton"]
	let secondButton = XCUIApplication().buttons["MenuBottomButton"]
	
	override func setUp() {
		super.setUp()
		UserDefaults.standard.set(false, forKey: "isDarkMode")
		continueAfterFailure = false
		app.launch()
		firstButton.tap()
	}
	
	
	func testCircleAsWinner(){
		let selectionSequence = [0,1,3,4,6]
		
		selectPlayersMove(selectionSequence: selectionSequence)
		
		let winnerLabel = app.staticTexts["gameWinnerLabel"].label
		let winnerImage = app.images["gameWinnerImage"]
		let gameCircleScore = app.staticTexts["gameCircleScore"].label
		let gameCrossScore = app.staticTexts["gameCrossScore"].label
		
		XCTAssertEqual(winnerLabel, "The Winner Is:")
		XCTAssert(winnerImage.exists)
		XCTAssertEqual(gameCircleScore, "1")
		XCTAssertEqual(gameCrossScore, "0")
	}
	
	func testCrossAsWinner(){
		let selectionSequence = [1,0,2,4,7,8]

		selectPlayersMove(selectionSequence: selectionSequence)
		
		let winnerLabel = app.staticTexts["gameWinnerLabel"].label
		let winnerImage = app.images["gameWinnerImage"]
		let gameCircleScore = app.staticTexts["gameCircleScore"].label
		let gameCrossScore = app.staticTexts["gameCrossScore"].label
		
		XCTAssertEqual(winnerLabel, "The Winner Is:")
		XCTAssert(winnerImage.exists)
		XCTAssertEqual(gameCircleScore, "0")
		XCTAssertEqual(gameCrossScore, "1")
	}
	
	func testDraw(){
		let selectionSequence = [0,1,3,4,7,6,2,5,8]

		selectPlayersMove(selectionSequence: selectionSequence)
		
		let winnerLabel = app.staticTexts["gameWinnerLabel"].label
		let gameCircleScore = app.staticTexts["gameCircleScore"].label
		let gameCrossScore = app.staticTexts["gameCrossScore"].label
		
		XCTAssertEqual(winnerLabel, "It's a Draw")
		XCTAssertEqual(gameCircleScore, "0")
		XCTAssertEqual(gameCrossScore, "0")
	}
	
	func testBackButton(){
		XCTAssert(app.otherElements["gameView"].isHittable)
		XCUIApplication().buttons["NavigationBarBackButton"].tap()
		XCTAssert(app.otherElements["menuView"].isHittable)
	}
	
	func testDarkModeButton(){
		let selectionSequence = [0,1,3,4,7,6,2,5,8]
		selectPlayersMove(selectionSequence: selectionSequence)
		
		let modeButton = app.navigationBars["1v1"]/*@START_MENU_TOKEN@*/.buttons["NavigationBarModeButton"]/*[[".buttons[\"lightMode\"]",".buttons[\"NavigationBarModeButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
		XCTAssertTrue(modeButton.isHittable)
		modeButton.tap()
		XCTAssertTrue(modeButton.isHittable)
		modeButton.tap()
	}
	
	
	private func selectPlayersMove(selectionSequence: [Int]){
		for selection in selectionSequence{
			app/*@START_MENU_TOKEN@*/.collectionViews/*[[".otherElements[\"gameView\"].collectionViews",".collectionViews"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .cell).element(boundBy: selection).children(matching: .other).element.tap()
			usleep(100000)
		}
	}
}
