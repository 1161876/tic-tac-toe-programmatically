//
//  MenuTests.swift
//  Tic_tac_toe_No_StoryboardUITests
//
//  Created by Tiago  Santos on 12/06/2020.
//  Copyright © 2020 Tiago  Santos. All rights reserved.
//

import XCTest
@testable import Tic_Tac_Toe

class MenuTests: XCTestCase {

	let app =  XCUIApplication()
	let firstButton = XCUIApplication().buttons["MenuTopButton"]
	let secondButton = XCUIApplication().buttons["MenuBottomButton"]

	override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }
	
	func testMenuAppearCorrectly(){
		XCTAssertEqual(firstButton.label , "1v1")
		XCTAssertEqual(secondButton.label, "Solo")
	}
	
	func testSelectMultiplePlayerSelection(){
		secondButton.tap()
		XCTAssertTrue(app.buttons["NavigationBarBackButton"].isHittable)
		XCTAssertEqual(firstButton.label , "Moderate")
		XCTAssertEqual(secondButton.label, "Impossible")
	}
	
	func testSinglePlayerSelection(){
		XCUIApplication().buttons["MenuTopButton"].tap()
		XCTAssert(app.otherElements["gameView"].isHittable)
		XCTAssertTrue(app.staticTexts["1v1"].exists)
	}
	
	func testBackButtonResetButtonValues(){
		secondButton.tap()
		XCTAssertEqual(firstButton.label , "Moderate")
		XCTAssertEqual(secondButton.label, "Impossible")
		app.buttons["NavigationBarBackButton"].tap()
		XCTAssertEqual(firstButton.label , "1v1")
		XCTAssertEqual(secondButton.label, "Solo")
	}
	
	func testModerateAISelection(){
		secondButton.tap()
		XCUIApplication().buttons["MenuTopButton"].tap()
		XCTAssertTrue(app.staticTexts["Moderate"].exists)
	}
	
	func testImpossibleAISelection(){
		secondButton.tap()
		XCUIApplication().buttons["MenuBottomButton"].tap()
		XCTAssertTrue(app.staticTexts["Impossible"].exists)
	}
}
